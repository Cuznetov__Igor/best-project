(function() {
    'use strict';

    angular
        .module('app')
        .controller('HomeCtrl', ['$http', '$state', HomeCtrl]);

    function HomeCtrl ($http, $state) {
        var self = this;
        self.obj = {};

        self.sendData = function sendData () {
            console.log(self.data);
            if (!self.data) {
                self.result = 'Please input a value';
            } else {
                self.obj = {
                    url: self.data
                };

                $http.post('/home', self.obj)
                    .then(function success (res) {
                        // console.log(res);
                        self.result = "http://localhost:3333/home/" + res.data;
                        console.log(self.result);
                    }, function error (err) {
                        console.log(err);
                    });
            }
        };


        self.getAllLinks = function getAllLinks () {
            $state.go('info');
        };

    }

}());
