(function() {
    'use strict';

    angular
        .module('app')
        .config(routes);

    function routes($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                controller: 'HomeCtrl',
                controllerAs: 'home',
                templateUrl: 'app/components/home/templates/home.tpl.html'
            })

            .state('info', {
                url: '/info',
                controller: 'InfoCtrl',
                controllerAs: 'info',
                templateUrl: 'app/components/info/templates/info.tpl.html'
            });
        $urlRouterProvider.otherwise('/home');
    }

}());
