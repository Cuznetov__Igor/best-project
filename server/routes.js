module.exports = function (app) {
    var home = require('./app/controllers/home');

    app.get('/home', home.getLinksApi);
    app.get('/home/:id', home.getLinkByIdApi);
    app.post('/home', home.addLinkApi);
    app.delete('/home/:id', home.deleteLinkById);
};
