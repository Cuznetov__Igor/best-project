var uuid = require('uuid');

var entrys = [
    {
    id: '110ec58a-a0f2-4ac4-8393-c866d813b8d1',
    url: 'youtube.com'
}];

module.exports.getLinksApi = function getLinksApi (req, res) {
    res.send(entrys);
};

module.exports.getLinkByIdApi = function getLinkByIdApi (req, res) {
    var url;
    var id = req.params.id;

    for (var i = 0; i < entrys.length; i++) {
        if (entrys[i].id == id) {
            url = entrys[i].url;
        }
    }

    if (url) {
        res.send('Your link is ' + url);
    } else {
        res.status(404).send('wrong');
    }
};

module.exports.addLinkApi = function addLinkApi (req, res) {
    var url = req.body.url;
    var id = uuid.v4();

    entrys.push({
        id: id,
        url: url
    });

    res.send(id);
};

module.exports.deleteLinkById = function deleteLinkById (req, res) {
    var id = req.params.id;
    var count = 0;

    for (var i = 0; i < entrys.length; i++) {
        if (entrys[i].id == id) {
            entrys.splice(i, 1);
            count++;
        }
    }

    if (count) {
        res.send('You successfully deleted link');
    } else {
        res.status(404).send('Link dosnt found');
    }
};
